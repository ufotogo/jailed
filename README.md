# README #

Jailed is a complete working app for android. This was a learning project for me.
It uses volley, has an update checker/installer, uses shared preferences, file io,
broadcast receiver, AppController, LruBitmapCache, a custom cache, intent sending,
and more. I had fun making it.

### This App works off the JailBase Api so you will need to sign up for an api key at Jalbase and replace the api_key in MainActivity.java  ###

* Jailed
* Version 2.0
* License - ASIS no warranty of any kind. Use at your own risk, and agree not to hold me
or anyone entity liable for anything that comes from using this app. feel free to use my code
as you wish, all contributed code will fall under its license.  

### How do I get set up? ###

* You can go to http://ufotogogames.com and get the apk
* Clone and import this repo into android studio
* Dependencies
* This app uses the volley lib and updatechecker.jar. See the Gradle file 
* 
* dependencies {
*   compile 'com.android.support:appcompat-v7:22.1.0'
*   compile files('libs/updatechecker.jar')
*   compile 'com.mcxiaoke.volley:library:1.0.19'
*    
* }
*
* NOTICE! At the top of an activity set debug = true for 
* a complete program flow through the logcat output 


### Contribution  ###

* https://github.com/RaghavSood/AppaholicsUpdateChecker.git
* https://github.com/mcxiaoke/android-volley.git
* https://developer.jailbase.com/

### Who do I talk to? ###

* Charles Wilson
* charleslynn1969@gmail.com