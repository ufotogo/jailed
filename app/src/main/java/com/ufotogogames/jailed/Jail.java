package com.ufotogogames.jailed;
/**
 * Android    Jailed
 * Sdk        minSdkVersion 12, targetSdkVersion 22
 *
 * @author Charles L Wilson
 *         Email      charles@ufotogogames.com
 *         Copyright  Copyright © Sep 09, 2015 By Charles L. Wilson All Rights Reserved.
 *         <p/>
 *         License    This program is free software: you can redistribute it and/or modify
 *         it under the terms of the GNU General Public License as published by
 *         the Free Software Foundation, either version 3 of the License, or
 *         (at your option) any later version.
 *         <p/>
 *         Disclaimer  This program is distributed in the hope that it will be useful,
 *         but WITHOUT ANY WARRANTY; without even the implied warranty of
 *         MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *         GNU General Public License for more details.
 */


import android.util.Log;

import org.json.JSONObject;


public class Jail {
    public static final String TAG = "@Jail";

    private String city;
    private String name;
    private String stateLong;
    private String address;
    private String url;
    private String county;
    private String phone;
    private String stateShort;
    private String id;
    private String zipCode;
    private String email;
    private String hasMugShot;


    public Jail(JSONObject object){
        Boolean debug = false;
        if (debug) Log.d(TAG, "Jail Source created");

        try{
            city = object.getString("city");
            if (city.equals("null")){city="??";}

            name = object.getString("name");
            if (name.equals("null")){name = "??";}

            stateLong = object.getString("state_full");
            if (stateLong.equals("null")){stateLong = "??";}

            address = object.getString("address1");
            if (address.equals("null")){address = "??";}

            url = object.getString("source_url");
            if (url.equals("null")){url="??";}

            county = object.getString("county");
            if (county.equals("null")){county = "??";}

            phone = object.getString("phone");
            if (phone.equals("null")){phone = "??";}

            stateShort = object.getString("state");
            if (stateShort.equals("null")){stateShort = "??";}

            id = object.getString("source_id");
            zipCode = object.getString("zip_code");
            if (zipCode.equals("null")){zipCode="??";}

            email = object.getString("email");
            if (email.equals("null")){email="??";}

            // convert true and false to yes and no mugshot
            hasMugShot = object.getString("has_mugshots");
            if (hasMugShot.equals("true")){
                hasMugShot = "yes";
            }else
            {
                hasMugShot = "No";
            }


        }catch (Exception e){
            Log.e(TAG,e.toString());
        }

    }
    // getters
    public String getCity(){return city;}
    public String getName(){return name;}
    public String getStateLong(){return stateLong;}
    public String getAddress(){return address;}
    public String getUrl(){return url;}
    public String getCounty(){return county;}
    public String getPhone(){return phone;}
    public String getStateShort(){return stateShort;}
    public String getMyJailCode(){return id;}
    public String getZipCode(){return zipCode;}
    public String getEmail(){return email;}
    public String getHasMugShot(){return hasMugShot;}
}
