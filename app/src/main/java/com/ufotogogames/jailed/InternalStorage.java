package com.ufotogogames.jailed;

import android.content.Context;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Android    Jailed
 * Sdk        minSdkVersion 12, targetSdkVersion 22
 *
 * @author Charles L Wilson
 *         Email      charles@ufotogogames.com
 *         Copyright  Copyright © Sep 09, 2015 By Charles L. Wilson All Rights Reserved.
 *         <p/>
 *         License    This program is free software: you can redistribute it and/or modify
 *         it under the terms of the GNU General Public License as published by
 *         the Free Software Foundation, either version 3 of the License, or
 *         (at your option) any later version.
 *         <p/>
 *         Disclaimer  This program is distributed in the hope that it will be useful,
 *         but WITHOUT ANY WARRANTY; without even the implied warranty of
 *         MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *         GNU General Public License for more details.
 */
public final class InternalStorage {

    private InternalStorage() {}

    public static void writeObject(Context context, String key, Object object) throws IOException {

        FileOutputStream fos = context.openFileOutput(key, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(object);
            oos.close();
            fos.close();
    }

    public static Object readObject(Context context, String key) throws IOException,
        ClassNotFoundException {
          FileInputStream fis = context.openFileInput(key);
          ObjectInputStream ois = new ObjectInputStream(fis);
          Object object = ois.readObject();
          return object;
    }
}