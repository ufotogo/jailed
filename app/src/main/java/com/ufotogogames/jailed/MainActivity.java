package com.ufotogogames.jailed;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.ufotogogames.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

/**
 * Android    Jailedv1.04 Released Sept. 9,2015
 * Sdk        minSdkVersion 12, targetSdkVersion 22
 *
 * @author Charles L Wilson
 *         Email      charles@ufotogogames.com
 *         Copyright  Copyright © Sep 09, 2015 By Charles L. Wilson All Rights Reserved.
 *         <p/>
 *         License    This program is free software: you can redistribute it and/or modify
 *         it under the terms of the GNU General Public License as published by
 *         the Free Software Foundation, either version 3 of the License, or
 *         (at your option) any later version.
 *         <p/>
 *         Disclaimer  This program is distributed in the hope that it will be useful,
 *         but WITHOUT ANY WARRANTY; without even the implied warranty of
 *         MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *         GNU General Public License for more details.
 */



public class MainActivity extends AppCompatActivity {
    private static String TAG = "@MainActivity -> ";
    public static final Boolean debug= false;

    /**
     * Jailbase Api Key
     * After signing up at https://developer.jailbase.com/
     * replace the api Key below with yours
     */
    public static final String API_KEY = "put your api_key here";

    public static final String JAILBASE_RECENT_ARREST_URL="https://api-2445581323150.apicast.io/api/1/recent/?source_id=";

    /** Used to change Volley's request time out
     *  The default was to short for this url
     */
    public static final int VOLLEY_DEFAULT_TIMEOUT = 20000;

    /* Activity request codes */
    public static final int REQUEST_NEW_JAIL =1;
    public static final String JSON_REQUEST_DONE = "makeJsonObjectRequest().DONE";
    public static final String JAIL_PREFERENCES = "Jail";
    public static final String JAIL_OLD_PREFERENCES = "SavedJail";

    /**
     * Things needed
     * */
    private ArrayList<Inmate> listOfInmates;
    private ArrayList<Inmate> tmpInmateList;
    public String urlJsonObj;
    private InmateAdapter inmateAdapter;
    private GetMoreInmatesReceiver receiver;
    private  ListView listView;
    private int index=0;
    private String jailCode;
    private String jailName;
    private String jailAddress;
    private String jailCity;
    private String jailState;
    private String jailPhone;
    private int pageToGet = 1; // default page
    private long cacheFileCreationTime = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (debug)Log.d(TAG,"onCreate() ");


    /* Check apk.ufotogogames.com server for an update and install it.
    * Below is the Git repository for the DownloadManager & UpdateChecker
    * https://github.com/Kiirani/AppaholicsUpdateChecker.wiki.git
    * Requires a text file containing the 1 digit version from the manifest
    * and the apk to be installed  on the server. If the installed apk version is smaller
    * than the version text file on the server. The installation will proceed.
    * Skip if not. Uses an Alert Dialog for asking to be installed.
    * */

        checkForUpdate();

    /**
     *  MAIN ACTIVITY
     */
       /**List for inmates to view, and a temp list
        *  for the ones that are being downloaded.,
        * */
        listOfInmates = new ArrayList<>();
        tmpInmateList = new ArrayList<>();

        /** Registers the receiver used to receive notifications
         *  from makeJsonObjectRequest() that it's onResponse has finished
         */

        IntentFilter intentFilter = new IntentFilter(JSON_REQUEST_DONE);
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        receiver = new GetMoreInmatesReceiver();
        registerReceiver(receiver, intentFilter);


     /** Initializes the app with filename.
      **/
        initialize(JAIL_PREFERENCES);

        if (debug)Log.d(TAG,"end of onCreate");
    }

    /**
     * Initializes the app with filename.
     *  @param  preferenceFile to load.
     *
     **/
    public void initialize(String preferenceFile){
        if (debug)Log.d(TAG,"initialize()");

        // Load stored preferences
        getPreferences(preferenceFile);
        // sets the jail info to be displayed
        setHeader();
        /* get the cache*/
        readCache();
        // checks to see if cache is to old to use.
        // and get new data
        checkCacheAge();
        //temp stuff
        //loadAllFromServer();
    }

    /**
     * Clears cache data and downloads new from server
     * */
    public void loadAllFromServer(){
        if (debug)Log.d(TAG,"loadAllFromServer()");


        pageToGet = 1;
        listOfInmates.clear();
        tmpInmateList.clear();
        writeCache();
        makeJsonObjectRequest();
    }
    /**
     * Check cache time, if it's past 1 hour old get new data
     *  1 hours = 3600000 milliseconds
     * */
public void checkCacheAge(){
    if (debug)Log.d(TAG,"checkCacheAge()");

    long currentTimeMillis = System.currentTimeMillis();
    if (debug)Log.d(TAG,"cache time passed: "+String.valueOf(currentTimeMillis-cacheFileCreationTime));
    if (currentTimeMillis - cacheFileCreationTime > 3600000) {
        // clear old for new
        listOfInmates.clear();
        loadAllFromServer();
    }
}

     /**
      * Loads the preference file. If there is no file
      * it will use the default values provided.
      * @param fileName - Name of the preference file to load.
      * */
    public void getPreferences(String fileName){
        if (debug)Log.d(TAG,"getPreferences()");

        SharedPreferences prefs = getSharedPreferences(fileName, MODE_PRIVATE);

            jailCode = prefs.getString("code","al-myso");
            jailName = prefs.getString("name", "Montgomery County Sheriff's Office");//"No name defined" is the default value.
            jailAddress = prefs.getString("address","225 South McDonough Street");
            jailCity = prefs.getString("city","Montgomery");
            jailState = prefs.getString("state","Al");
            jailPhone = prefs.getString("phone","334-832-4980");
        // Setup url with jail code and page 1
        urlJsonObj = JAILBASE_RECENT_ARREST_URL+jailCode+"&page=1&user_key="+API_KEY;
    }

    /**
     * Writes the preferences to filename
     * @param  fileName - Preference file to write to.
     * */
    public void writeSharedPreferences(String fileName){
        if (debug)Log.d(TAG,"writeSharedPreferences()");

        SharedPreferences pref = getApplicationContext().getSharedPreferences(fileName, MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("code", jailCode);
        editor.putString("name",jailName);
        editor.putString("address",jailAddress);
        editor.putString("city", jailCity);
        editor.putString("state", jailState);
        editor.putString("phone",jailPhone);
        editor.apply();
    }

    /**
     * Sets up the header of the main list view page
     * */
    public void setHeader(){
        if (debug)Log.d(TAG,"setHeader()");

        TextView title = (TextView)findViewById(R.id.tvJailTitle);
        title.setText(jailName);
        TextView address = (TextView)findViewById(R.id.tvJailAddress);
        address.setText(jailAddress);
        TextView city = (TextView)findViewById(R.id.tvJailCity);
        city.setText(jailCity);
        TextView state = (TextView)findViewById(R.id.tvJailState);
        state.setText(jailState);
        TextView phone = (TextView)findViewById(R.id.tvJailPhone);
        phone.setText(jailPhone);
    }


    /**
     * Displays (listOfInmates) inmate list in the  ListView.
     * */

    public void displayListView() {
        if (debug) Log.d(TAG, "displayListView() ");

        listView = (ListView) findViewById(R.id.listView1);
        inmateAdapter = new InmateAdapter(this, listOfInmates);
        listView.setAdapter(inmateAdapter);
        inmateAdapter.notifyDataSetChanged();
        /* Restores the list position after pause(), or app exit()*/
        listView.post(new Runnable() {
            @Override
            public void run() {
                listView.setSelection(index);
            }
        });

         /* onClickListener to select list item.*/
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Inmate inmate = (Inmate) inmateAdapter.getItem(position);

                /* Send Intent to load selection into browser*/
                String url = inmate.getMoreInfoUrl();
                if (url != null) {
                   if (debug)Log.d(TAG, url);
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);
                }else {
                    toast("No more information available.");
                }
            }
        });
    }

    /**
     *  Makes Volley request for url and decodes json results into tmpInmateList.
     *  Toast's a progress dialog of page that is being downloaded.
     */
    private void makeJsonObjectRequest() {
        if (debug)Log.d(TAG,"makeJsonObjectRequest()");


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.GET,
                urlJsonObj, new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {
                if (debug)Log.d(TAG,"makeJsonObjectRequest() -> onResponse()");
                if (debug)Log.d(TAG,"Url: "+urlJsonObj);


                try {

                    /* Parsing json object response */
                    String status = response.getString("status");
                    if (debug)Log.d(TAG, "Status-> " + status);
                    String errorMessage = response.getString("msg");

                    /* Test for good response*/
                    if (status.equals("1")) {
                        /*get inmate records  array object*/
                        JSONArray records = response.getJSONArray("records");

                        /* Test - if got some records*/
                        if (records.length() == 0){
                            toast("No records found!\n Returning to previous Jail.\n");

                            /* No records found, Restore previous preferences*/
                            /* Do not reload from server  listOfInmates = tmpInmateList;*/
                            if (debug)Log.d(TAG,"makeJsonObjectRequest-> No records found()");

                            initialize(JAIL_OLD_PREFERENCES);
                            /* Restore preferences that were changed in settings*/
                            writeSharedPreferences(JAIL_PREFERENCES);
                            /* No cache to restore loadAllFromServer*/
                            loadAllFromServer();
                            return;

                        }else {
                            /* Iterate through array and get objects*/
                            for (int i = 0; i < records.length(); i++) {
                                JSONObject obj = records.getJSONObject(i);
                                /**
                                 * Load objects for viewing into a temp list.
                                 * The broad cast receiver will move the temp list to,
                                 * the to be displayed list after all pages are loaded.
                                 * */
                                 tmpInmateList.add(new Inmate(obj));
                            }
                        }

                    }else {
                        toast("API Load error." + status + "-> " + errorMessage+"\n\n Select from Settings.");
                        return;
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    toast(e.getMessage());
                }

                sendResultsBackBroadCast();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Volley Response Error! ");
                toast("Network Error!\n " +
                        "Check Internet connection.\n"+
                        "Url maybe offline.\n" +
                        "Try again later.");
            }
        });{
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                    VOLLEY_DEFAULT_TIMEOUT,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        }

        /* Add the  request to request queue.*/
        AppController.getInstance().addToRequestQueue(jsonObjReq);
        /* Increment page counter*/
        pageToGet++;
    }


    /**
     *  Called after Settings selection has been made.
     *  */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (debug)Log.d(TAG,"SettingsActivity -> onActivityResult");

        /* After selecting a new jail in settings activity, load and display the new jail.*/
        if (requestCode == REQUEST_NEW_JAIL && resultCode == RESULT_OK){

            initialize(JAIL_PREFERENCES);
            loadAllFromServer();
        }
    }

    /**
     * Load cache files, If not found, They will be created.
     * */
    public void readCache(){
        if (debug)Log.d(TAG,"readCache");

        try {
            /* clear old data.*/
            listOfInmates.clear();
            listOfInmates = (ArrayList<Inmate>) InternalStorage.readObject(this, "list");
            index = (Integer) InternalStorage.readObject(this,"position");
            cacheFileCreationTime = (Long) InternalStorage.readObject(this,"time");
            /* If cache has been cleared loadAllFromServer()*/
            if (debug)Log.d(TAG,"Total records read in from cache: "+String.valueOf(listOfInmates.size()));

        } catch (IOException e) {
            e.printStackTrace();
            toast("Creating new cache file.");
            Log.e(TAG,e.getMessage());

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            toast("Cache file not found, Creating new.");
            Log.e(TAG,e.getMessage());
        }
    }

    /**
     * Write the cache files.
     * */
    public void writeCache(){
        if (debug)Log.d(TAG,"writeCache()");
        /*  Try to get the list position.*/
        try{
            index = listView.getFirstVisiblePosition();
        }catch (Exception e){
           /* could not get position, set default.*/
            index = 0;
        }
        if (debug)Log.d(TAG,"Total records wrote to cache: "+String.valueOf(listOfInmates.size()));
        /* get the file write time.*/
        long time= System.currentTimeMillis();
        /* cache the list.*/
        try {
            InternalStorage.writeObject(this,"list",listOfInmates);
            InternalStorage.writeObject(this,"position",index);
            InternalStorage.writeObject(this,"time",time);
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG,e.getMessage());
            toast("file write error");
        }
    }

    @Override
    protected void onPause() {
        if (debug)Log.d(TAG,"onPause()");
        writeSharedPreferences(JAIL_PREFERENCES);
        writeCache();
        super.onPause();
    }

    @Override
    protected void onResume() {
        if (debug)Log.d(TAG,"onResume()");
        restoreCache();
        displayListView();
        super.onResume();
    }
    /**
     * Load cache and check it's age.
     * If it's to old, checkCacheAge() will call for new load from server.
     * */
    public void restoreCache(){
        if (debug)Log.d(TAG,"restoreCache()");
        /* load cache file.*/
        readCache();
        /* check to see if cache is to old , if so, reload from server.*/
        checkCacheAge();
    }

    @Override
    protected void onStop() {
        if (debug)Log.d(TAG,"onStop() -> not used");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        if (debug)Log.d(TAG,"onDestroy");
        this.unregisterReceiver(receiver);
        super.onDestroy();
    }

    /**
     * This receiver class handles the data after makeJsonObjectRequest()-> onResponse is done..
     * */
    public class GetMoreInmatesReceiver extends  BroadcastReceiver{
        private static final String TAG = "@BroadcastReceiver() ";

        @Override
        public void onReceive(Context context, Intent intent) {
           if (debug)Log.i(TAG," onReceive() Action Received by Broadcast Receiver.");
           if (debug)Log.i(TAG," finished page "+String.valueOf(pageToGet-1));

            /* let user know what is happening.*/
           toast("Loading page "+String.valueOf(pageToGet-1)+" of 10");

            /*display when all are loaded.*/
            if (pageToGet == 11){
                listOfInmates = tmpInmateList;
                if (debug)Log.d(TAG,"listOfInmates at receiver: "+String.valueOf(listOfInmates.size()));
                if (debug)Log.d(TAG,"tmpInmateList at receiver: "+String.valueOf(tmpInmateList.size()));
                writeCache();
                displayListView();
            }

            /**
             * Delay 1.5 seconds before making another request to the server.
             * JailBase API policy requires a wait of 1 sec.
             * */
            if (pageToGet <= 10){

                new CountDownTimer(1500,1000){
                    @Override
                    public void onTick(long millisUntilFinished) {
                    }

                    @Override
                    public void onFinish() {

                        /*Make sure not to exceed total number of 10 pages available.*/
                        if (pageToGet >10 )return;
                        String pageNumber = String.valueOf(pageToGet);
                        urlJsonObj = JAILBASE_RECENT_ARREST_URL+jailCode+"&page="+pageNumber+"&user_key="+API_KEY;
                        if (debug)Log.d(TAG,"loading page...."+ pageNumber);
                        makeJsonObjectRequest();
                    }
                }.start();
            }

        }
    }
    /**
     * Check for newer version apk.
     * */
    public void checkForUpdate(){
        final UpdateChecker checker = new UpdateChecker(this, true);
       if (debug)Log.d(TAG,"checkForUpdate()");

        /* listen for checker results.*/
        checker.addObserver(new Observer() {
            public void update(Observable o,Object data) {
                if(checker.isUpdateAvailable()) {
                    /* Ask to do update.*/
                    askToUpdate(checker);
                    Log.d(TAG,"Asking to do update");
                }
            }
        });

        /* Check for new version apk.*/
        checker.checkForUpdateByVersionCode("http://apk.ufotogogames.com/apk/jailed/jailed.txt");
        /* Log no update available.*/
        if (!checker.isUpdateAvailable()) {
            if (debug)Log.d(TAG,"No Update available.");
        }
    }

    /**
     *  Ask to update apk.
     *  @param  checker checks for newer apk then installs it
     */
    public void askToUpdate(final UpdateChecker checker){
        if (debug)Log.d(TAG,"askToUpdate()");

        new AlertDialog.Builder(this)
                .setTitle("New updates available!")
                .setMessage("Install them now?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        /* Continue with the installation.*/
                        Log.d(TAG,"Installing new apk.");
                        checker.downloadAndInstall("http://apk.ufotogogames.com/apk/jailed/jailed.apk");
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        /* Do nothing.*/
                        Log.d(TAG,"Install canceled");
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    /**
     * Send broadcast makeJsonObject() -> response is done.
     * */
    public void sendResultsBackBroadCast()    {
        if (debug)Log.d(TAG,"broadCastIntent() ");

        Intent intent = new Intent();
        intent.setAction(JSON_REQUEST_DONE);
        this.sendBroadcast(intent);
    }

    /**
     *  Toast message to screen.
     * @param message Message to display
     * */
    public void toast(String message){
        if (debug)Log.d(TAG,"toast()");
        if (message != null) {
            Toast.makeText(getApplicationContext(),
                    message, Toast.LENGTH_SHORT).show();
        }
    }

    /* Standard Action bar stuff.*/
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        /* Inflate the menu; this adds items to the action bar if it is present.*/
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /**
         * Handle action bar item clicks here. The action bar will
         * automatically handle clicks on the Home/Up button, so long
         * as you specify a parent activity in AndroidManifest.xml.
         */
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            /* Launch settings activity.*/
            Intent i = new Intent(this,SettingsActivity.class);
            startActivityForResult(i,REQUEST_NEW_JAIL);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}



