package com.ufotogogames.jailed;

/**
 * Android    Jailed
 * Sdk        minSdkVersion 12, targetSdkVersion 22
 *
 * @author Charles L Wilson
 *         Email      charles@ufotogogames.com
 *         Copyright  Copyright © Sep 09, 2015 By Charles L. Wilson All Rights Reserved.
 *         <p/>
 *         License    This program is free software: you can redistribute it and/or modify
 *         it under the terms of the GNU General Public License as published by
 *         the Free Software Foundation, either version 3 of the License, or
 *         (at your option) any later version.
 *         <p/>
 *         Disclaimer  This program is distributed in the hope that it will be useful,
 *         but WITHOUT ANY WARRANTY; without even the implied warranty of
 *         MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *         GNU General Public License for more details.
 */

import android.graphics.Bitmap;
import android.support.v4.util.LruCache;
import android.util.Log;

import com.android.volley.toolbox.ImageLoader.ImageCache;

public class LruBitmapCache extends LruCache<String, Bitmap> implements
        ImageCache {
    public static final String TAG = "@LruBitmapCache ";
    public static final Boolean debug = false;

    public static int getDefaultLruCacheSize() {
        if (debug) Log.d(TAG, "LruBitmapCache created ");
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        final int cacheSize = maxMemory / 8;
        return cacheSize;
    }

    public LruBitmapCache() {
        this(getDefaultLruCacheSize());
        if (debug)Log.d(TAG,"LruBitmapCache() ");
    }

    public LruBitmapCache(int sizeInKiloBytes) {
        super(sizeInKiloBytes);
    }

    @Override
    protected int sizeOf(String key, Bitmap value) {
        if (debug)Log.d(TAG,"sizeOf() ");
        return value.getRowBytes() * value.getHeight() / 1024;
    }

    @Override
    public Bitmap getBitmap(String url) {
        if (debug)Log.d(TAG,"getBitmap() ");
        return get(url);
    }

    @Override
    public void putBitmap(String url, Bitmap bitmap) {
        if (debug)Log.d(TAG,"putBitmap() ");
        put(url, bitmap);
    }
}