package com.ufotogogames.jailed;
import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.ufotogogames.app.AppController;

import java.util.List;

/**
 * Android    Jailed
 * Sdk        minSdkVersion 12, targetSdkVersion 22
 *
 * @author Charles L Wilson
 *         Email      charles@ufotogogames.com
 *         Copyright  Copyright © Sep 09, 2015 By Charles L. Wilson All Rights Reserved.
 *         <p/>
 *         License    This program is free software: you can redistribute it and/or modify
 *         it under the terms of the GNU General Public License as published by
 *         the Free Software Foundation, either version 3 of the License, or
 *         (at your option) any later version.
 *         <p/>
 *         Disclaimer  This program is distributed in the hope that it will be useful,
 *         but WITHOUT ANY WARRANTY; without even the implied warranty of
 *         MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *         GNU General Public License for more details.
 */





public class InmateAdapter extends BaseAdapter {
    public static final Boolean debug = false;

    private static final String TAG = "@InmateAdapter-> ";
    public static final String GET_MORE_INMATES = "get";
    private Activity activity;
    private LayoutInflater inflater;
    private List<Inmate> inmateItems;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();


    // Constructor
    public InmateAdapter(Activity activity, List<Inmate> inmateItemsList) {
        if (debug)Log.d(TAG,"inmateAdapter created");
        this.activity = activity;
        this.inmateItems = inmateItemsList;
    }

    @Override
    public int getCount() {
        if (debug)Log.d(TAG,"getCount() ");
            return inmateItems.size();
    }

    @Override
    public Object getItem(int location) {
        if (debug)Log.d(TAG,"getItem() ");
        return inmateItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        if (debug)Log.d(TAG,"getItemId() ");
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (debug)Log.d(TAG,"getView() ");

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.activity_main_list_items, parent, false);


        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();

        TextView name = (TextView)convertView.findViewById(R.id.tvName);
        TextView book_date = (TextView) convertView.findViewById(R.id.tvDateBooked);
        TextView charges = (TextView) convertView.findViewById(R.id.tvCharges);
        NetworkImageView mug_shot = (NetworkImageView) convertView.findViewById(R.id.ivMugShot);

        // getting data for the row
        Inmate inmate = inmateItems.get(position);

        // Load mugshot image
        mug_shot.setImageUrl(inmate.getMugShotUrl(), imageLoader);

        // Populate the data into the template view using the data object
        name.setText(inmate.getName());
        book_date.setText(inmate.getBookDate());
        charges.setText(inmate.getCharges());
        // Return the view to render on screen
        return convertView;
    }
}
