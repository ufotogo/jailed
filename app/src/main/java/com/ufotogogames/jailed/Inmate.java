package com.ufotogogames.jailed;
/**
 * Android    Jailed
 * Sdk        minSdkVersion 12, targetSdkVersion 22
 *
 * @author Charles L Wilson
 *         Email      charles@ufotogogames.com
 *         Copyright  Copyright © Sep 09, 2015 By Charles L. Wilson All Rights Reserved.
 *         <p/>
 *         License    This program is free software: you can redistribute it and/or modify
 *         it under the terms of the GNU General Public License as published by
 *         the Free Software Foundation, either version 3 of the License, or
 *         (at your option) any later version.
 *         <p/>
 *         Disclaimer  This program is distributed in the hope that it will be useful,
 *         but WITHOUT ANY WARRANTY; without even the implied warranty of
 *         MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *         GNU General Public License for more details.
 */


import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class Inmate  implements Serializable {
    public static final String TAG = "Inmate ";
    public static final Boolean debug = false;
    private String name;
    private String book_date;
    private String charges;
    private String mug_shot_url;
    private String more_info_url;

    // Constructor to convert JSON object into a Java class instance
    public Inmate(JSONObject object){
        if (debug) Log.d(TAG, "@Inmate created");
        try {
            this.name = object.getString("name");
            this.book_date = object.getString("book_date_formatted");
            // get charges array
            JSONArray charges = object.getJSONArray("charges");

            // get charges string
//            TODO - fix array out of bounds err when empty

            /**
             * Set default charges to not found
             * then if the charges array is not null
             * get the charges
             */
            this.charges = "No charges found";

            if (charges != null) {
                try {
                    this.charges = charges.getString(0);

                }catch (ExceptionInInitializerError error){
                    Log.e(TAG,"Charges Init Error! "+error.getMessage());
                    this.charges = "No charges listed.";
                }
            }


            this.mug_shot_url = object.getString("mugshot");
            this.more_info_url = object.getString("more_info_url");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Getters
     * @return  value
     */
    public String getMugShotUrl(){
        if (debug)Log.d(TAG,"getMugShotUrl() ");
        return mug_shot_url;
    }
    public String getMoreInfoUrl(){
        if (debug)Log.d(TAG,"getMoreInfoUrl() ");
        return more_info_url;
    }
    public String getName(){
        if (debug)Log.d(TAG,"getName() ");
        return name;
    }
    public String getCharges(){
        if (debug)Log.d(TAG,"getCharges() ");
        return charges;
    }
    public String getBookDate(){
        if (debug)Log.d(TAG,"getBookDate() ");
        return book_date;}
}