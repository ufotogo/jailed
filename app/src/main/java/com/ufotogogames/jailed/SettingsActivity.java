package com.ufotogogames.jailed;
/**
 * Android    Jailed
 * Sdk        minSdkVersion 12, targetSdkVersion 22
 *
 * @author Charles L Wilson
 *         Email      charles@ufotogogames.com
 *         Copyright  Copyright © Sep 09, 2015 By Charles L. Wilson All Rights Reserved.
 *         <p/>
 *         License    This program is free software: you can redistribute it and/or modify
 *         it under the terms of the GNU General Public License as published by
 *         the Free Software Foundation, either version 3 of the License, or
 *         (at your option) any later version.
 *         <p/>
 *         Disclaimer  This program is distributed in the hope that it will be useful,
 *         but WITHOUT ANY WARRANTY; without even the implied warranty of
 *         MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *         GNU General Public License for more details.
 */


import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.ufotogogames.app.AppController;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


public class SettingsActivity extends ActionBarActivity {
    public static final String TAG = "@Settings ";
    public static final Boolean debug = false;
    private static final String urlSources = "https://api-2445581323150.apicast.io:443/api/1/sources/?user_key=0469534e965580f4d4d2cacce9d56744";
    public static final int VOLLEY_DEFAULT_TIMEOUT = 20000;
    public static final String JAIL_PREFERENCES = "Jail";
    public static final String JAIL_OLD_PREFERENCES = "SavedJail";
    private ArrayList<Jail> jailSourceList;
    private String jailCode;
    private String jailName;
    private String jailPhone;
    private String jailAddress;
    private String jailCity;
    private String jailState;
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (debug)Log.d(TAG,"AppSettings-> onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_settings);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading.....");

        jailSourceList = new ArrayList<>();
        makeSettingsRequest();
    }

    public void displayListView(){
        if (debug)Log.d(TAG,"AppSettings -> displayListView");

       final SettingsAdapter settingsAdapter = new SettingsAdapter(this, jailSourceList);
        ListView listViewSettings = (ListView) findViewById(R.id.listViewSettings);
        listViewSettings.setAdapter(settingsAdapter);
        settingsAdapter.notifyDataSetChanged();

        // handle selected item
        listViewSettings.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> listView, View view,
                                    int position, long id) {
                if (debug)Log.d(TAG,"setOnClickListener");
                Jail myJail = (Jail) settingsAdapter.getItem(position);

                jailCode = myJail.getMyJailCode();
                jailName = myJail.getName();
                jailAddress = myJail.getAddress();
                jailCity = myJail.getCity();
                jailState = myJail.getStateShort();
                jailPhone = myJail.getPhone();
               // save selected preferences
                writeSharedPreferences(JAIL_PREFERENCES);
                finishWithResult();
            }
        });


        // Filter selection
        EditText settingsFilter = (EditText) findViewById(R.id.etSettingsFilter);
        settingsFilter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                settingsAdapter.getFilter().filter(s.toString());
            }
        });

    }

    public void writeSharedPreferences(String fileName){

        //
        SharedPreferences pref = getApplicationContext().getSharedPreferences(fileName, MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("code", jailCode);
        editor.putString("name",jailName);
        editor.putString("address",jailAddress);
        editor.putString("city", jailCity);
        editor.putString("state", jailState);
        editor.putString("phone",jailPhone);
        editor.apply();
    }

  public void makeSettingsRequest(){
      if (debug)Log.d(TAG,"AppSettings -> makeSettingsRequest");
     progressDialog.show();

      JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
              urlSources,  new Response.Listener<JSONObject>() {

          @Override
          public void onResponse(JSONObject response) {
              if (debug)Log.d(TAG,"AppSettings -> makeSettingsRequest-> onResponse");

              try {

                  String status = response.getString("status");
                  String message = response.getString("msg");
                  JSONArray records = response.getJSONArray("records");

                  if (status.equals("1")){
                        for (int i =0;i<records.length();i++) {
                            JSONObject obj = records.getJSONObject(i);
                            jailSourceList.add(new Jail(obj));
                        }
                      if (debug)Log.d(TAG,"Got Jail list.");
                  }else{
                      progressDialog.dismiss();
                      toast("API Load error.\n" + status + "-> " + message+"\n\n Load page failed.");
                      return;

                  }

              }catch (Exception error){
                  Log.d(TAG,error.getMessage());
                  toast(error.getMessage());
              }

            displayListView();
              progressDialog.dismiss();

          }
      }, new Response.ErrorListener() {
          @Override
          public void onErrorResponse(VolleyError volleyError) {
              VolleyLog.e("Volley Response Error! ");
              progressDialog.dismiss();
              toast("Network Error!\n " +
                      "Are you connected to the Internet?"+
              " Press Back, Fix the connection then " +
                      "try again.");
          }
      });{
          jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                  VOLLEY_DEFAULT_TIMEOUT,
                  DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                  DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
      }
      // Adding request to request queue
      AppController.getInstance().addToRequestQueue(jsonObjReq);
  }

    // Options menu stuff
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_app_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void toast(String message){
        if (message != null) {
            Toast.makeText(getApplicationContext(),
                    message, Toast.LENGTH_LONG).show();
        }
    }



    public void finishWithResult(){
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        this.finish();
    }
}

