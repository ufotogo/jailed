package com.ufotogogames.jailed;

/**
 * Android    Jailed
 * Sdk        minSdkVersion 12, targetSdkVersion 22
 *
 * @author Charles L Wilson
 *         Email      charles@ufotogogames.com
 *         Copyright  Copyright © Sep 09, 2015 By Charles L. Wilson All Rights Reserved.
 *         <p/>
 *         License    This program is free software: you can redistribute it and/or modify
 *         it under the terms of the GNU General Public License as published by
 *         the Free Software Foundation, either version 3 of the License, or
 *         (at your option) any later version.
 *         <p/>
 *         Disclaimer  This program is distributed in the hope that it will be useful,
 *         but WITHOUT ANY WARRANTY; without even the implied warranty of
 *         MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *         GNU General Public License for more details.
 */


import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class SettingsAdapter extends BaseAdapter {
    public static final String TAG = "@SettingsAdapter ";
    public static final Boolean debug = false;

    private Activity activity;
    private LayoutInflater inflater;
    private List<Jail> originalData =null;
    private List<Jail> savedList=null;
    private Filter filter;

    // Constructor
    public SettingsAdapter(Activity activity, List<Jail> jailItemsList) {
        if (debug) Log.d(TAG, "SettingsAdapter Created");
        this.activity = activity;
        this.originalData = jailItemsList;
        savedList = new ArrayList<>();
        savedList.addAll(jailItemsList);
    }

    @Override
    public int getCount() {
        if (debug) Log.d(TAG, "getCount() ");
        return originalData.size();
    }

    @Override
    public Object getItem(int position) {
        if (debug) Log.d(TAG, "getItem() ");
        return originalData.get(position);
    }

    @Override
    public long getItemId(int position) {
        if (debug) Log.d(TAG, "getItemId() ");
        return position;
    }

    public Filter getFilter() {
        if (debug)Log.d(TAG,"getFilter()");
        if (filter == null){
            filter  = new MyFilter();
            if (debug)Log.d(TAG,"MyFilter Created.");
        }
        return filter;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (debug) Log.d(TAG, "SettingsAdapter-> getView()");

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.activity_settings_list_items,parent,false);

        TextView name = (TextView) convertView.findViewById(R.id.tvJailName);
        TextView address = (TextView) convertView.findViewById(R.id.tvJailAddress);
        TextView city = (TextView) convertView.findViewById(R.id.tvJailCity);
        TextView state = (TextView) convertView.findViewById(R.id.tvJailState);
        TextView phone = (TextView) convertView.findViewById(R.id.tvJailPhone);
        TextView hasMug = (TextView) convertView.findViewById(R.id.tvJailHasMugshot);
        // get jail obj from list
        Jail jail = originalData.get(position);
        // set the text with getters from jail obj
        name.setText(jail.getName());
        address.setText(jail.getAddress());
        city.setText(jail.getCity());
        state.setText(jail.getStateLong());
        phone.setText(jail.getPhone());
        hasMug.setText(jail.getHasMugShot());

        return convertView;
    }

    private class MyFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (debug)Log.d(TAG,"preformFiltering()");
            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<Jail> list = originalData;

            int count = list.size();
            final ArrayList<Jail> nlist = new ArrayList<>(count);

            String filterableString ;

            for (int i = 0; i < count; i++) {
                filterableString = list.get(i).getStateShort();
                if (filterableString.toLowerCase().contains(filterString)) {
                    nlist.add(list.get(i));
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }


        @Override
        protected void publishResults(CharSequence constraint, Filter.FilterResults results) {
            if (debug)Log.d(TAG,"publishResults()");

            originalData = (ArrayList<Jail>) results.values;
            notifyDataSetChanged();
            // test something then pit back org list
            //originalData.clear();
            originalData.addAll(savedList);
            notifyDataSetInvalidated();
        }
    }
}

